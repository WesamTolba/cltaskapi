<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    protected $fillable = [
        'patient_id', 'doctor_id', 'desc', 'pain_id', 'date', 'time', 'confirmed'
    ];

    public function doctorName()
    {
        return $this->belongsTo('App\User','doctor_id');
        
    }

    public function patientName()
    {
        return $this->belongsTo('App\User','patient_id');
              
    }
}
