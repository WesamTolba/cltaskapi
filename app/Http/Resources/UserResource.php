<?php

namespace App\Http\Resources;

use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'username'   => $this->username,
            'first_name' => $this->first_name,
            'last_name'  => $this->last_name,
            'email'      => $this->email,
            'mobile'     => $this->mobile,
            'birth_date' => $this->birth_date,
            'gender'     => $this->gender == 1 ? 'Male' : 'Female',
            'country'    => $this->country,
            'occupation' => $this->occupation,
            'pain_id'    => $this->pain_id,
        ];
    }
}
