<?php

namespace App\Http\Resources;

use App\User;
use Carbon\Carbon;
use App\Appointment;
use Illuminate\Http\Resources\Json\JsonResource;

class AppointmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'patient_id' => $this->patient_id,
            'doctor_id'  => $this->doctor_id,
            'desc'       => $this->desc,
            'date'       => Carbon::parse($this->date)->format('d-M-Y'),
            'time'       => Carbon::parse($this->time)->format('h:i'),
            'confirmed'  =>$this->confirmed
        ];
    }
}
