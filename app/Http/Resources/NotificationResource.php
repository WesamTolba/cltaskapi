<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'message' => $this->data['body'],
            'seen' => $this->read_at ? true : false,
            'id' => $this->id,
            'created_at' => $this->created_at,
        ];
    }
    
}
