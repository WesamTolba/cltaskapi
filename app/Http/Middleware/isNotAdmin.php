<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class isNotAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        foreach(auth()->user()->roles as $role) {

            if(Auth::check() && $role->pivot->role_id == 1){
                return redirect(route('admin'));
            }
        }
        return $next($request);
    }
}
