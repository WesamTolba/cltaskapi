<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;


class isAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        foreach(auth()->user()->roles as $role) {

            if(Auth::check() && $role->pivot->role_id == 2){
                return redirect(route('userHome'));
            }
            elseif(Auth::check() && $role->pivot->role_id == 3){
                return redirect(route('userHome'));
            }
        }

        return $next($request);
    }
}
