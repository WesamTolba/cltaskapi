<?php

namespace App\Http\Controllers\Api;

use App\PainList;
use Illuminate\Http\Request;

class PainListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PainList  $painList
     * @return \Illuminate\Http\Response
     */
    public function show(PainList $painList)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PainList  $painList
     * @return \Illuminate\Http\Response
     */
    public function edit(PainList $painList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PainList  $painList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PainList $painList)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PainList  $painList
     * @return \Illuminate\Http\Response
     */
    public function destroy(PainList $painList)
    {
        //
    }
}
