<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\UserRole;
use App\Appointment;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserCollection;
use App\Http\Resources\AppointmentResource;
use App\Notifications\appointmentNotification;
use App\Http\Resources\NotificationResource;

class UserController extends Controller
{
    public function personalInformation(UserRequest $request)
    {

        $user = auth()->user();
        $userRole = UserRole::where('user_id', $user->id)->get();
        
        if(count($userRole) > 0)
        {
            UserRole::where('user_id', $user->id)->update([
                'role_id' => $request->role_id,
            ]);
        }

        UserRole::updateOrCreate([
            'user_id' => $user->id,
            'role_id' => $request->role_id,
        ]);

        $user->update([

                'first_name' => $request->first_name,
                'last_name'  => $request->last_name,
                'email'      => $request->email,
                'mobile'     => $request->mobile,
                'birth_date' => $request->birth_date,
                'gender'     => $request->gender,
                'country'    => $request->country,
                'occupation' => $request->occupation,
                'pain_id'    => $request->pain_id,
                'confirmed'  => 1
        ]);

        $token = JWTAuth::fromUser($user);
        return response()->json(['status' => 'success','User' => new UserResource(auth()->user()),'Token' => $token], 200);
    }

    public function getAppointments()
    {
        
        foreach(auth()->user()->roles as $role) {
            
            if($role->pivot->role_id == 2){
                 $appointments = Appointment::Where('doctor_id',auth()->user()->id)->get();
            }
            else{
                 $appointments = Appointment::Where('patient_id',auth()->user()->id)->get();
            }
        }

        return response()->json(['status' => 'success','Appointments'=>  AppointmentResource::collection($appointments)], 200);
    }

    public function confirmed($id)
    {
        $appointment = Appointment::findOrfail($id);

        
        $appointment->update([
            'confirmed' =>  !$appointment->confirmed
        ]);
        if($appointment->confirmed == 1){
            return response()->json(['status' => 'success','Appointments'=>  'Confirmed'], 200);
        }else {
            $user = User::find(1);
            $user->notify(new appointmentNotification());
            return response()->json(['status' => 'success','Appointments'=>  'Rejected and we notify to admin and he will reschedule the appointment or change the doctor and assign the case to another doctor, please keep on'], 200);
        }
    }

    public function unreadNotifications()
    {
        return response()->json(['status' => 'success','Notifications'=>  NotificationResource::collection(auth()->user()->notifications)], 200);
       
    }

    public function notification($id) {
        
        if(auth()->user()->notifications()->find($id)->read_at == NULL)
        {
          $read_notification = auth()->user()->notifications()->find($id)->markAsRead();
          return response()->json(['status' => '1','message'=>'success','Notification' => 'seen'],200);
        }
        else{
          $unread_notification = auth()->user()->notifications()->find($id)->markAsUnRead();
          return response()->json(['status' => '1','message'=>'success','Notification' => 'Unread'],200);
        }   
    }
}
