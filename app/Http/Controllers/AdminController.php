<?php

namespace App\Http\Controllers;

use App\User;
use App\Appointment;
use Illuminate\Http\Request;
use App\Http\Requests\AppoinmentRequest;
use App\Notifications\appointmentNotification;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $count        = 1;
        $users        = User::get();
        $appointments = Appointment::get();
        return view('admin.index',compact('count','users','appointments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function assignAppointment(AppoinmentRequest $request)
    {
        Appointment::create([
            'patient_id' => $request->patient_id,
            'doctor_id'  => $request->doctor_id,
            'desc'       => $request->desc,
            'date'       => $request->date,
            'time'       => $request->time,
        ]);
        
        $doctor = User::find($request->doctor_id);
        $doctor->notify(new appointmentNotification());

        $patient = User::find($request->patient_id);
        $patient->notify(new appointmentNotification());

        return redirect()->back();
    }

    public function notification($id) {

        if(auth()->user()->notifications()->find($id)->read_at == NULL)
        {
            $read_notification = auth()->user()->notifications()->find($id)->markAsRead();
            auth()->user()->notifications()->find($id)->markAsRead();
            return redirect()->back();
        }
        else{
            $unread_notification = auth()->user()->notifications()->find($id)->markAsUnRead();
            auth()->user()->notifications()->find($id)->markAsRead();
            return redirect()->back();
        }
        
    }
    
}
