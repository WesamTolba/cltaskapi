<?php

use Illuminate\Database\Seeder;

class PainListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\PainList::class, 25)->create();
    }
}
