<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
            'username'   => 'admin',
            'first_name' => 'Super',
            'last_name'  => 'Admin',
            'email'      => 'admin@cl.com',
            'mobile'     => '01001112233',
            'birth_date' => '1994-8-15',
            'gender'     => 'Male',
            'country'    => 'Egypt',
            'password'   => '$2y$10$BAyJSuw61kSise7c4JhqtuXVgqok/GyIpVrcAPwEqVmtnM7cUDXNi', //12345678
            'occupation' => 'Software developer',
            'confirmed'  => 1
    ];
});
