<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::group(['middleware' => ['auth','isAdmin']],function(){
    
    // Default home page
    Route::get('/home', 'HomeController@index')->name('home');
    // Index for admin user
    Route::get('/admin', 'AdminController@index')->name('admin');
    // Assign an appointment
    Route::post('/assignAppointment', 'AdminController@assignAppointment')->name('assignAppointment');
    // Seen notifications
    Route::get('/notifications/{id}', 'AdminController@notification')->name('notifications');
});