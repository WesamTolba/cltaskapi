<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->group(function () {
    // Register with JWT
    Route::post('register', 'Api\AuthController@register');
    // Login with JWT
    Route::post('login', 'Api\AuthController@login');
    // Refresh JWTtoken
    Route::get('refresh', 'Api\AuthController@refresh');

    Route::group(['middleware' => 'auth:api'], function(){
        // get user
        Route::get('user', 'Api\AuthController@getUser');
        // logout
        Route::post('logout', 'Api\AuthController@logout');
        // Save personal information data
        Route::post('personalInformation', 'Api\UserController@personalInformation');
        // Get user's appointments
        Route::get('/getAppointments', 'Api\UserController@getAppointments');
        // Confirm or declined appoinment
        Route::get('/confirmed/{id}','Api\UserController@confirmed');
        // User's notifications
        Route::get('/unreadNotifications','Api\UserController@unreadNotifications');
        // Seen notifications
        Route::get('/notifications/{id}', 'api\UserController@notification');
    });
});