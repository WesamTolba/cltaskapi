@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">List  of patients</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">First name</th>
                                <th scope="col">Last name</th>
                                <th scope="col">Pain</th>
                                <th scope="col">Assign to doctor</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)

                                @foreach ($user->roles as $role)


                                    <tr>
                                        @if($role->pivot->role_id != 2 && $role->pivot->role_id != 1)
                                            <th scope="row">{{$user->id}}</th>
                                            <td>{{$user->first_name}}</td>
                                            <td>{{$user->last_name}}</td>
                                            @if ($user->painList != NULL)
                                                <td>{{$user->painList->name}}</td>
                                            @endif
                                            <td> <button class="btn btn-info modal-save" type="button" data-toggle="modal" data-target="#assignDoctor{{$user->id}}" style="margin-bottom:0;">Assign to doctors</button></td>                              
                                        @endif
                                    </tr>

                                @endforeach
                            @endforeach
                        </tbody>
                    </table>
                </div>

            @foreach($users as $user)
                <div class="modal fade" id="assignDoctor{{$user->id}}" role="dialog">
                    <div class="modal-dialog edit-city-modal">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <section class="block">
                                    <h5 style="margin-bottom:10px;">Assign to doctor</h5>
                                    <form method="POST" action="{{ route('assignAppointment') }}">
                                        @csrf  
                                        <div class="row">
                                            <div class=" col-sm-12">
                                                <label for="inputType1">Patient name </label>
                                                <div class="form-group row showcase_row_area">
                                                    <input type="text" class="form-control" name="patient_name"  id="editusername{{$user->id}}" value="{{$user->username}}" dir="auto" style="text-align: left;" readonly>
                                                <input type="hidden" name="patient_id" value="{{$user->id}}">  
                                                @error('patient_name')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class=" col-sm-12">
                                                <label for="inputType1">Doctors</label>
                                                <div class="form-group row showcase_row_area">
                                                    <select style="height: 50px;" name="doctor_id" class="form-control" required >
                                                                <option value="" selected>{{ __('Select a doctor') }}</option>
                                                        @foreach ($users as $user)
                                                            @foreach ($user->roles as $role)
                                                                @if($role->pivot->role_id != 3 && $role->pivot->role_id != 1)
                                                                    <option value="{{$user->id}}" {{ old('doctor_id') == $user->id ? 'selected' : ''}}>{{ $user->first_name}}</option>
                                                                @endif
                                                            @endforeach
                                                        @endforeach
                                                    </select>
                                                        @error('doctor_id')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class=" col-sm-12">
                                                <label for="inputType1">Description</label>
                                                <div class="form-group row showcase_row_area">
                                                    <textarea id="desc" name="desc" class="form-control" rows="4">{{old('desc')}} </textarea>
                                                    @error('desc')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class=" col-sm-12">
                                                <label for="inputType1">Time</label>
                                                <div class="form-group row showcase_row_area">
                                                    <input type="time" name="time" class="form-control">
                                                    @error('time')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class=" col-sm-12">
                                                <label for="inputType1">Date</label>
                                                <div class="form-group row showcase_row_area">
                                                    <input type="date" name="date" class="form-control">
                                                    @error('date')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="footer-btn">
                                            <button class="btn btn-info modal-save" type="submit">Assign</button>
                                        </div>
                                    </form>   
                                </section>

                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
                </div>
                <div class="card" style="margin-top:2rem;"> 
                    
                    <div class="card-header">List  of appointments</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Patient name</th>
                                    <th scope="col">Doctor name</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Time</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Reschedule</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($appointments as $appointment)
                                        <tr>
                                            <th scope="row">{{$appointment->id}}</th>
                                            <td>{{$appointment->patientName->username}}</td>
                                            <td>{{$appointment->doctorName->username}}</td>
                                            <td>{{Carbon\Carbon::parse($appointment->date)->format('d-M-Y')}}</td>
                                            <td>{{Carbon\Carbon::parse($appointment->time)->format('h:i')}}</td>
                                            @if ($appointment->confirmed == 0)
                                            <td><button class="btn btn-dark" disabled="disabled">Declined</button></td> 
                                            <td><button class="btn btn-warning" data-toggle="modal" data-target="#assignDoctor{{$user->id}}">Reschedule</button></td>
                                            @else 
                                            <td><button class="btn btn-success" disabled="disabled">Confirmed</button></td>
                                            <td><button class="btn btn-dark" disabled="disabled">Approved</button></td>  
                                            @endif
                                            
                                        </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    </div>
</div>
@endsection
